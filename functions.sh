#!/bin/bash

function find_image() {
	local one=$1
	if [ ${#one} -lt 1 ]; then
		echo "image name error !"
		return
	fi
	
	# docker image list --format "table {{.ID}}\t{{.Repository}}\t{{.Tag}}\t{{.Size}}"
	local gres=$(docker image list --format "table {{.Repository}}:{{.Tag}}" | grep "$one")
	if [ "$gres" == "$one" ]; then
		echo "$one"
		return
	else
		echo ""
		return
	fi
}

function build_image() {

	# build_image tor-privoxy-debian:1.1 ./image/tor-privoxy-debian/
	
	local image_name=$1
	img=$(find_image ${image_name})
	if [ "$img" == "${image_name}" ]; then
		echo "image $img already exists !"
		return
	fi
	local dockerfile_path=$2
	DockerAdd=" "
	if [ "$OSTYPE" == "msys" ] || [ "$OSTYPE" == "win32" ]; then
		# for windows with bash you can add "winpty" before commands
		DockerAdd=" winpty "
	fi
	test_tor_proxy
	$DockerAdd docker build --network=host --build-arg http_proxy=http://127.0.0.1:8118 --build-arg https_proxy=http://127.0.0.1:8118 --build-arg no_proxy=127.0.0.1,127.0.0.0/8 -t $image_name $dockerfile_path
}

function build_image_nocache() {

	# build_image tor-privoxy-debian:1.1 ./image/tor-privoxy-debian/

	local image_name=$1
	local dockerfile_path=$2
	DockerAdd=" "
	if [ "$OSTYPE" == "msys" ] || [ "$OSTYPE" == "win32" ]; then
		# for windows with bash you can add "winpty" before commands
		DockerAdd=" winpty "
	fi
	test_tor_proxy
	$DockerAdd docker build --network=host --no-cache --build-arg http_proxy=http://127.0.0.1:8118 --build-arg https_proxy=http://127.0.0.1:8118 --build-arg no_proxy=127.0.0.1,127.0.0.0/8 -t $image_name $dockerfile_path
}

function docker_remove_all() {

	docker-compose stop || true
	docker-compose rm -f || true

	docker network prune -f || true
	docker stop $(docker ps -a -q) || true
	docker kill $(docker ps -a -q) || true
	docker rm -vf $(docker ps -a -q) || true
	docker network prune -f || true
	docker stop $(docker ps -a -q) || true
	docker kill $(docker ps -a -q) || true
	docker rm -vf $(docker ps -a -q) || true
	docker network prune -f || true
	docker stop $(docker ps -a -q) || true
	docker kill $(docker ps -a -q) || true
	docker rm -vf $(docker ps -a -q) || true
	
}

function docker_remove_image() {

	local one=$1
	docker stop $one || true
	docker kill $one || true
	docker rm -vf $one || true
	docker image rm $one || true
	
}

# find_build_image
# find_build_image tor-router:1.0
# find_build_image tor-router-debian:1.1

function test_tor_proxy_simple() {
	curl_res=""
	curl_res=$(curl --retry-connrefused --connect-timeout 240 --retry 3 --retry-delay 30 -x 127.0.0.1:8118 -S -s https://check.torproject.org/api/ip | grep :true | head -c 13)
	if [ "$curl_res" == "{\"IsTor\":true" ]; then
		echo "Tor proxy OK !"
		return
	else
		echo "Tor proxy KO !"
		return
	fi
}

function test_tor_proxy() {
	local restart=$1
	curl_res=$(test_tor_proxy_simple)
	if [ "$curl_res" == "Tor proxy OK !" ]; then
		echo "Tor proxy OK !"
		return
	else
		echo "Tor proxy KO !"
		local findres=$(find_image tor-privoxy-debian:1.1)
		if [ "$findres" == "tor-privoxy-debian:1.1" ]; then
			echo "Image tor-privoxy-debian:1.1 found"
			if [ "$restart" != "restart" ]; then
				echo "Try to restart proxy container !"
				docker-compose stop tor-privoxy-debian
				docker-compose up -d tor-privoxy-debian
				test_tor_proxy restart
				return
			else
				echo "ERROR: Image may be corrupted, try to rebuild it !"
				return
			fi
		else
			echo "Image tor-privoxy-debian:1.1 not found !"
			build_tor_proxy
			if [ "$restart" != "restart" ]; then
				test_tor_proxy restart
			else
				echo "ERROR: rebuild and stard failed !"
				return
			fi
			return
		fi
		return
	fi
}

function build_tor_proxy() {
	local findres=$(find_image tor-privoxy-debian:1.1)
	if [ "$findres" == "tor-privoxy-debian:1.1" ]; then
		echo "Image tor-privoxy-debian:1.1 found"
		return
	fi
	
	local findres=$(find_image tor-privoxy:1.0)
	if [ "$findres" != "tor-privoxy:1.0" ]; then
		echo "Image tor-privoxy:1.0 not found > BUILD"
		docker-compose build --no-cache tor-privoxy
	fi
		
	docker-compose stop tor-privoxy
	docker-compose up -d tor-privoxy
	test_tor_proxy_simple
	
	dirpath=$(pwd)
	dirpath=${dirpath##*/}
	# if used in submodule
	if [ ${dirpath} != "tor-router" ]; then
		dirpath=$(git rev-parse --show-toplevel)/tor-router
	fi
	docker build --network=host --no-cache --build-arg http_proxy=http://127.0.0.1:8118 --build-arg https_proxy=http://127.0.0.1:8118 --build-arg no_proxy=127.0.0.1,127.0.0.0/8 -t tor-privoxy-debian:1.1 ${dirpath}/image/tor-privoxy-debian/
	docker-compose stop tor-privoxy
	docker-compose up -d tor-privoxy-debian
	test_tor_proxy_simple
	
	return
}
