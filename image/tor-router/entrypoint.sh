#!/bin/bash

if [ -f "/root/scripts/startup.sh" ]; then
  . /root/scripts/startup.sh
fi

# Block all trafic
iptables -F
iptables -X
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP
# ip6tables -F
# ip6tables -X
# ip6tables -P INPUT DROP
# ip6tables -P FORWARD DROP
# ip6tables -P OUTPUT DROP

# Configure environment variables
TOR_ROUTER_USER="${TOR_ROUTER_USER:-tor-router}"
TOR_ROUTER_UID="${TOR_ROUTER_UID:-9001}"
TOR_ROUTER_HOME="${TOR_ROUTER_HOME:-/opt/tor-router}"
TOR_CONFIG_FILE="${TOR_CONFIG_FILE:-${TOR_ROUTER_HOME}/torrc}"
SCRIPTS_HOME="${SCRIPTS_HOME:-/opt/tor-router}"

# Enable debug if requested
if [ "${DEBUG}" = "true" ]; then
  set -x
fi
echo 'Verifying environment'

# TODO: Detect --net=host and fail

# Check for CAP_NET_ADMIN
if ! iptables -nL &> /dev/null; then
  >&2 echo 'Container requires CAP_NET_ADMIN, add using `--cap-add NET_ADMIN`.'
  exit 1
fi

# Ensure that the container only has eth0 and lo to start with
for interface in $(ip link show | awk '/^[0-9]*:/ {print $2}' | sed -e 's/:$//' -e 's/@.*$//'); do
  if [ "$interface" != "lo" ] && [ "$interface" != "eth0" ]; then
    >&2 echo 'Container should only have the `eth0` and `lo` interfaces'
    >&2 echo 'Additional interfaces should only be added once tor has been started'
    >&2 echo 'Killing to avoid accidental clobbering'
    # exit 1
  fi
done

echo 'Setting up container'

# Setup the TOR_ROUTER_USER
chown -R ${TOR_ROUTER_UID}:${TOR_ROUTER_UID} ${TOR_ROUTER_HOME}
adduser --disabled-password --shell /bin/bash --home "${TOR_ROUTER_HOME}" --uid "${TOR_ROUTER_UID}" --gecos "${TOR_ROUTER_USER}" "${TOR_ROUTER_USER}"

# bash
# Rstr

iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F && iptables -X && iptables-legacy-restore "${SCRIPTS_HOME}/iptables.rules"
# ip6tables -P INPUT ACCEPT
# ip6tables -P FORWARD ACCEPT
# ip6tables -P OUTPUT ACCEPT
# ip6tables -F && ip6tables -X && ip6tables-legacy-restore "${SCRIPTS_HOME}/ip6tables.rules"

echo "nameserver 127.0.0.1" > /etc/resolv.conf

# Run tor as the TOR_ROUTER_USER
echo 'Starting the Tor router'
exec sudo -u "${TOR_ROUTER_USER}" tor -f "${TOR_CONFIG_FILE}" "$@"




# Generate IPTables rules exemple

# TOR_UID=9001
# TRANS_PORT="9040"


# iptables -F
# iptables -t nat -F
# iptables -t nat -A OUTPUT -m owner --uid-owner 9001 -j RETURN
# iptables -t nat -A OUTPUT -p udp --dport 53 -j REDIRECT --to-ports 5353

# iptables -t nat -A OUTPUT -d 10.192.0.0/10 -p tcp --syn -j REDIRECT --to-ports 9040

# NON_TOR="10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 127.0.0.0/8"
# for NET in $NON_TOR; do
	# iptables -t nat -A OUTPUT -d $NET -j RETURN
# done

# iptables -t nat -A OUTPUT -p tcp --syn -j REDIRECT --to-ports 9040
# iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# for NET in $NON_TOR; do
	# iptables -A OUTPUT -d $NET -j ACCEPT
# done
  
# iptables -A OUTPUT -m owner --uid-owner 9001 -j ACCEPT
# iptables -A OUTPUT -j ACCEPT

# iptables-save > iptables.rules

# ip6tables -F
# ip6tables -t nat -F
# ip6tables -t nat -A OUTPUT -m owner --uid-owner 9001 -j RETURN
# ip6tables -t nat -A OUTPUT -p udp --dport 53 -j REDIRECT --to-ports 5353

# ip6tables -t nat -A OUTPUT -d FC00::/7 -p tcp --syn -j REDIRECT --to-ports 9040

# NON_TOR="fd00::/8 fe80::/10 ::1"
# for NET in $NON_TOR; do
	# ip6tables -t nat -A OUTPUT -d $NET -j RETURN
# done

# ip6tables -t nat -A OUTPUT -p tcp --syn -j REDIRECT --to-ports 9040
# ip6tables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# for NET in $NON_TOR; do
	# ip6tables -A OUTPUT -d $NET -j ACCEPT
# done
  
# ip6tables -A OUTPUT -m owner --uid-owner 9001 -j ACCEPT
# ip6tables -A OUTPUT -j ACCEPT

# ip6tables-save > ip6tables.rules