FROM debian:bullseye-slim

# Protection, only work if PROXY is set successfully
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y curl
RUN curl -S -s https://check.torproject.org/api/ip | grep :true, || exit 1

COPY ./service /etc/service/

EXPOSE 8118 9050

RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y gpg apt-transport-https ca-certificates

COPY ./tor.list /etc/apt/sources.list.d/tor.list


RUN curl -L -S -s http://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null && DEBIAN_FRONTEND=noninteractive apt update && apt install -y tor deb.torproject.org-keyring privoxy obfs4proxy tor torsocks runit tini git --no-install-recommends \
&& addgroup --system tordocker \
&& adduser --system tordocker --ingroup tordocker \
&& chown tordocker:tordocker /etc/service \
&& chown -R tordocker:tordocker /etc/service/* \
&& apt autoremove -y && apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Debian CIS apply tests
RUN git clone https://github.com/ovh/debian-cis.git && \
	cd debian-cis && \
	cp debian/default /etc/default/cis-hardening && \
	sed -i "s#CIS_ROOT_DIR=.*#CIS_ROOT_DIR='$(pwd)'#" /etc/default/cis-hardening && \	
	bin/hardening.sh --sudo --allow-unsupported-distribution --apply

HEALTHCHECK --interval=120s --timeout=15s --start-period=120s --retries=2 \
            CMD curl --fail -x http://127.0.0.1:8118 -s 'https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion' -k > /dev/null && echo "HealthCheck succeeded..." || exit 1

USER tordocker

ENTRYPOINT ["tini", "--"]
CMD ["runsvdir", "/etc/service"]