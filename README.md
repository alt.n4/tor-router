# Network TOR Router for docker-compose projects  
  
Theses container / services will help you to route throught TOR other services, other container or the build process of Docker as you need them.  
  
  
  
## Images
  
Base image is made with Alpine, the -debian images are made with Debian instead
  
## with docker compose  
  
### Build    
  
```bash
docker-compose build --no-cache tor-router
```
or
```bash
docker-compose build --no-cache tor-router-debian
```

## Usages  
  
```bash
docker-compose up --remove-orphans tor-router
docker-compose up --remove-orphans client


docker-compose up --remove-orphans tor-router-debian
docker-compose up --remove-orphans client-debian
```
You can run only "client" and it will launch the router if not launched.  
  
## with docker only  
  
### Build    
  
```bash
docker build -t tor-router:1.0 ./image/tor-router/
```
or
```bash
docker build -t tor-router-debian:1.0 ./image/tor-router-debian/
```

## Usages  
  
```bash
docker run --rm -d --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 tor-router:1.0
```
You have to wait for the TOR Bootstrapping to be 100%
```bash
docker logs tor-router
```
then you can use an image with network linked to route your traffic
```bash
docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router tor-router:1.0 sh -c "curl -S -s https://check.torproject.org/api/ip"
docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router tor-router:1.0 sh -c "curl -v -L -S -s http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion"
```
and you have to stop it later...  
```bash
docker stop tor-router
```

or

```bash
docker run --rm -d --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 tor-router-debian:1.0
```
You have to wait for the TOR Bootstrapping to be 100%
```bash
docker logs tor-router
```
then you can use an image with network linked to route your traffic
```bash
docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router tor-router-debian:1.0 sh -c "curl -S -s https://check.torproject.org/api/ip"
docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router tor-router-debian:1.0 sh -c "curl -v -L -S -s http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion"
```
and you have to stop it later...  
```bash
docker stop tor-router
```
  
## Bugs  
  
- Alpine linux 3.17.1 has DNS resolution bugs (when using local DNS) so if you link it to the gateway it may not work...
You can use version 3.11.2 and it will works  
  
## Privoxy  

```bash
docker-compose build --no-cache tor-privoxy
```
or 
```bash
docker build -t tor-privoxy:1.0 ./image/tor-privoxy/
```
Launch with :  
```bash
docker-compose up -d tor-privoxy
```
or  
```bash
docker run --name='tor-privoxy' -d \
  -p 9050:9050 \
  -p 9051:9051 \
  -p 8118:8118 \
tor-privoxy:1.0
```
  
test with :  
```bash
curl -v -x http://127.0.0.1:8118 -s 'https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion' -k
```
### Docker throught Privoxy ?  

Now you have an HTTP(S) proxy, you can change you're Docker config to use a proxy :  
change your `~/.docker/config.json` file to :  
```bash
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://127.0.0.1:8118",
     "noProxy": "127.0.0.0/8"
   }
 }
}
```
  
Now you're build will be created throught TOR  
  
If you have launch the tor-privoxy, you can test to build tor-privoxy-debian wich is configured to use proxy with docker compose :  
```bash
docker-compose build --no-cache tor-privoxy-debian
docker-compose build --no-cache --build-arg http_proxy=http://127.0.0.1:8118 --build-arg https_proxy=http://127.0.0.1:8118 --build-arg no_proxy=127.0.0.1,127.0.0.0/8 tor-privoxy-debian
```
or you can use directly build args from docker command :  
```bash
docker build --network=host --no-cache --build-arg http_proxy=http://127.0.0.1:8118 --build-arg https_proxy=http://127.0.0.1:8118 --build-arg no_proxy=127.0.0.1,127.0.0.0/8 -t tor-privoxy-debian:1.0 ./image/tor-privoxy-debian/
```
This docker image is protected and will work only if proxy is set correctly, you can test to remove your `~/.docker/config.json` and launch :  
```bash
docker build --no-cache -t tor-privoxy-debian:1.0 ./image/tor-privoxy-debian/
```
and it will fail  
  
to protect Debian images you can use :
```bash
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y curl
RUN curl https://check.torproject.org/api/ip | grep :true, || exit 1
```
or for Alpine images you can use :
```bash
RUN apk update --no-cache && apk upgrade -a --no-cache && apk --update --no-cache add curl \
RUN curl  -S -s https://check.torproject.org/api/ip | grep :true, || exit 1
```
