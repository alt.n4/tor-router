#!/bin/bash

set -e

source ./functions.sh

docker-compose stop

docker_remove_image tor-router-debian:1.0 || true
docker_remove_image tor-router-debian:1.1 || true
docker_remove_image tor-privoxy-debian:1.0 || true
docker_remove_image tor-privoxy-debian:1.1 || true
docker_remove_image client-debian:1.0 || true

build_tor_proxy
test_tor_proxy

build_image_nocache tor-router-debian:1.1 ./image/tor-router-debian/
build_image client-debian:1.0 ./image/tor-router-debian/

test_tor_proxy

DockerAdd=" "
if [ "$OSTYPE" == "msys" ] || [ "$OSTYPE" == "win32" ]; then # for windows with bash you can add "winpty" before commands
	DockerAdd=" winpty "
fi

$DockerAdd docker run --rm -d --name tor-router-debian --cap-add NET_ADMIN --dns 127.0.0.1 tor-router-debian:1.1
$DockerAdd sleep 10
$DockerAdd docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router-debian client-debian:1.0 sh -c "curl --retry-connrefused --connect-timeout 240 --retry 20 --retry-delay 5 -S -s https://check.torproject.org/api/ip | grep :true"
$DockerAdd docker stop tor-router-debian

$DockerAdd docker-compose up client-debian
$DockerAdd docker-compose stop tor-router-debian

echo "All tests are succesfull !"
