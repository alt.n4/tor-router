#!/bin/bash

set -e

source ./functions.sh

docker-compose stop

docker_remove_image tor-router:1.0 || true
docker_remove_image tor-privoxy:1.0 || true
docker_remove_image client:1.0 || true

docker-compose build --no-cache tor-privoxy
		
docker-compose stop tor-privoxy
docker-compose up -d tor-privoxy
	
test_tor_proxy

build_image_nocache tor-router:1.0 ./image/tor-router/
build_image client:1.0 ./image/tor-router/

DockerAdd=" "
if [ "$OSTYPE" == "msys" ] || [ "$OSTYPE" == "win32" ]; then # for windows with bash you can add "winpty" before commands
	DockerAdd=" winpty "
fi

$DockerAdd docker run --rm -d --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 tor-router:1.0
sleep 10
$DockerAdd docker run -it --rm --volume=$(pwd)/image/tor-router-debian/scripts:/root/scripts --network=container:tor-router client:1.0 sh -c "curl --retry-connrefused --connect-timeout 240 --retry 20 --retry-delay 5 -S -s https://check.torproject.org/api/ip | grep :true"

$DockerAdd docker stop tor-router

docker-compose up client
docker-compose stop tor-router

echo "All tests are succesfull !"
